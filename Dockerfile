FROM golang:1-alpine
WORKDIR /app
COPY go.mod .
COPY *.go .
EXPOSE 4000

RUN mkdir bin
## Windows
RUN env GOOS=windows GOARCH=amd64 go build -v -o ./bin/server_windows-1.0.exe
## Linux
RUN env GOOS=linux GOARCH=386 go build -v -o ./bin/server_linux-1.0
## macos
RUN env GOOS=darwin GOARCH=amd64 go build -v -o ./bin/server_macos-1.0

CMD ["./bin/server_linux-1.0"]
